*** Darwin 8x2 ***
Turn = 0.
  01
0 ..
1 .h
2 ..
3 f.
4 fr
5 ff
6 .r
7 th

Turn = 1.
  01
0 ..
1 .h
2 ..
3 f.
4 fr
5 fr
6 .r
7 tr

Turn = 2.
  01
0 ..
1 .h
2 ..
3 f.
4 fr
5 fr
6 .r
7 rr

Turn = 3.
  01
0 ..
1 .h
2 ..
3 f.
4 rr
5 fr
6 r.
7 rr

*** Darwin 5x6 ***
Turn = 0.
  012345
0 .....r
1 h.....
2 ....f.
3 h.....
4 hh....

Turn = 3.
  012345
0 ......
1 ...h..
2 ....fr
3 h.....
4 h...h.

*** Darwin 4x6 ***
Turn = 0.
  012345
0 h.rfrh
1 hrt..t
2 ..r...
3 ...f.h

Turn = 14.
  012345
0 rr.rrr
1 ..r..r
2 ....rt
3 ...rrr

*** Darwin 8x2 ***
Turn = 0.
  01
0 h.
1 h.
2 ..
3 h.
4 h.
5 ..
6 t.
7 .t

Turn = 6.
  01
0 h.
1 h.
2 ..
3 .h
4 h.
5 ..
6 t.
7 .t

Turn = 12.
  01
0 h.
1 h.
2 ..
3 .h
4 h.
5 ..
6 t.
7 .t

*** Darwin 4x5 ***
Turn = 0.
  01234
0 t.r..
1 .....
2 hhfh.
3 .h.r.

Turn = 4.
  01234
0 t..hr
1 .....
2 h.f..
3 .h.hr

Turn = 8.
  01234
0 t..h.
1 .....
2 h.f.r
3 .hrr.

Turn = 12.
  01234
0 t..rr
1 ...r.
2 hrrr.
3 .....

*** Darwin 8x8 ***
Turn = 0.
  01234567
0 ......hf
1 ..t.....
2 ..r...t.
3 ........
4 ........
5 ...r....
6 .......h
7 ........

Turn = 5.
  01234567
0 ......hf
1 ..t.....
2 .....tt.
3 ........
4 ........
5 ........
6 .......h
7 .r......

Turn = 10.
  01234567
0 ......hf
1 ..t.....
2 .....tt.
3 ........
4 ........
5 ........
6 r......h
7 ........

Turn = 15.
  01234567
0 ......hf
1 r.t.....
2 .....tt.
3 ........
4 ........
5 ........
6 .......h
7 ........

*** Darwin 7x3 ***
Turn = 0.
  012
0 .f.
1 ...
2 t.h
3 ...
4 ...
5 ...
6 .r.

Turn = 1.
  012
0 .f.
1 ...
2 th.
3 ...
4 ...
5 ...
6 ..r

Turn = 2.
  012
0 .f.
1 ...
2 th.
3 ...
4 ...
5 ...
6 ..r

Turn = 3.
  012
0 .f.
1 ...
2 th.
3 ...
4 ...
5 ...
6 ..r

*** Darwin 2x2 ***
Turn = 0.
  01
0 rf
1 rf

Turn = 7.
  01
0 rr
1 rr

Turn = 14.
  01
0 rr
1 rr

*** Darwin 8x7 ***
Turn = 0.
  0123456
0 .......
1 .......
2 .......
3 .......
4 .......
5 .......
6 .....h.
7 f....r.

Turn = 6.
  0123456
0 .......
1 .......
2 .......
3 .......
4 .......
5 .......
6 .......
7 rr...h.

*** Darwin 5x8 ***
Turn = 0.
  01234567
0 .f......
1 ..rt..h.
2 .r...r..
3 h..ft...
4 ......t.

Turn = 12.
  01234567
0 ...r.rr.
1 ....t..r
2 ....t...
3 ....tt..
4 ...r.t..

*** Darwin 4x7 ***
Turn = 0.
  0123456
0 ..t...h
1 ....h.h
2 ......h
3 h......

Turn = 4.
  0123456
0 h.t...h
1 .....hh
2 ......h
3 .......

Turn = 8.
  0123456
0 h.t...h
1 .....hh
2 ......h
3 .......

Turn = 12.
  0123456
0 h.t...h
1 .....hh
2 ......h
3 .......

*** Darwin 6x7 ***
Turn = 0.
  0123456
0 ......t
1 .......
2 .....r.
3 ..f....
4 f..r..f
5 ...f...

Turn = 5.
  0123456
0 ...r.r.
1 ......r
2 .......
3 ..f....
4 f.....f
5 ...f...

Turn = 10.
  0123456
0 .......
1 ......r
2 .......
3 ..f...r
4 f......
5 ...f.rr

*** Darwin 8x5 ***
Turn = 0.
  01234
0 ..hf.
1 .....
2 t....
3 .....
4 fr...
5 ....r
6 ....f
7 .....

Turn = 5.
  01234
0 h..f.
1 .....
2 t....
3 .....
4 f....
5 r....
6 ....f
7 r....

*** Darwin 2x6 ***
Turn = 0.
  012345
0 r.t...
1 .h.hh.

Turn = 2.
  012345
0 rht...
1 ..t..h

*** Darwin 7x3 ***
Turn = 0.
  012
0 hr.
1 .h.
2 ...
3 f..
4 f.h
5 f.h
6 ..h

Turn = 4.
  012
0 r.h
1 r..
2 .r.
3 f..
4 f..
5 f.h
6 ..h

Turn = 8.
  012
0 ..h
1 ..r
2 r..
3 f..
4 f..
5 f.h
6 .rh

Turn = 12.
  012
0 r..
1 ...
2 .rr
3 ..r
4 f..
5 r.h
6 r.h

Turn = 16.
  012
0 ..r
1 ..r
2 r..
3 r..
4 .rr
5 ..r
6 .rr

*** Darwin 3x6 ***
Turn = 0.
  012345
0 t.f...
1 ...tr.
2 ..f.hr

Turn = 1.
  012345
0 t.f...
1 ...tt.
2 ..fh.r

*** Darwin 6x8 ***
Turn = 0.
  01234567
0 ....hh..
1 ftf.t..r
2 .f....h.
3 .......r
4 .th..f..
5 rt......

Turn = 3.
  01234567
0 ....th..
1 ttf.t...
2 rf.....h
3 ........
4 .tt..f.r
5 .t.....r

*** Darwin 4x6 ***
Turn = 0.
  012345
0 .rf...
1 r..tfh
2 h.tr.r
3 tf.hf.

Turn = 9.
  012345
0 ...ttt
1 ...ttt
2 rrrt..
3 .rttt.

*** Darwin 4x5 ***
Turn = 0.
  01234
0 .....
1 .thh.
2 ...t.
3 ..h..

Turn = 5.
  01234
0 ..h..
1 .t.t.
2 ...t.
3 ....h

*** Darwin 3x5 ***
Turn = 0.
  01234
0 .hft.
1 .....
2 .ffr.

Turn = 3.
  01234
0 ..rr.
1 .h.r.
2 .ff..

*** Darwin 3x4 ***
Turn = 0.
  0123
0 .hr.
1 h...
2 fhff

Turn = 4.
  0123
0 .h..
1 h..r
2 fhrr
